# flutter
This guide provides instructions on how to run a Flutter project

## Prerequisites
Before running the program, ensure that you have the following software installed on your computer:

- Flutter SDK: https://docs.flutter.dev/get-started/install
- Android Studio or IntelliJ IDEA: https://developer.android.com/

### Setup
1. If you plan to build the project on an Android emulator, you need to setup the Android SDK:
   - 1.1 Open Android Studio
   - 1.2 Choose tab Preferences or Settings > Search with key: Android SDK > Install the Android SDK version you need
2. If you plan to build the project on an iOS emulator, you need to setup Xcode. Download here: https://xcodereleases.com/
3. If you plan to build the project on Web application, you need to setup the environment variable for Web.
If you've installed the Flutter SDK manually, it will be automatically generated. 
You can verify this by using the command: `flutter devices`. If the list includes any browser entry, you can skip this step
4. Open the project and wait for the IDE setup process
5. Open the terminal and navigate to the project directory or use the terminal integrated within the IDE
6. Run the command: `flutter doctor -v` to check for the dependencies
7. To test the app, use the command `flutter run` in the terminal

#### Conclusion
Congratulations! You have successfully set up a new Flutter project on your computer.
You can now start building your app by modifying the files in the lib directory
If you encounter any issues or have any questions, we recommend checking out the official Flutter documentation https://docs.flutter.dev/ for help.
You can also reach out to the developer for assistance